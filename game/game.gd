extends Node2D

var helicopter = preload("res://objects/hellicopter/helicopter.tscn")

@export
var score = 0
var landedtroopers = 0
var airspace = [null, null]

func _ready():
	Signals.connect("trooperLand", trooperLanded)
	Signals.connect("hitTarget", hitTarget)
	Signals.connect("shot", shot)

func shot():
	if score - 1 >= 0:
		score -= 1
		$Score.text = str(score)

func hitTarget(landed: bool):
	if landed:
		landedtroopers -= 1
	score += 2
	$Score.text = str(score)

func trooperLanded():
	landedtroopers += 1
	if landedtroopers > 10:
		Signals.emit_signal("gameOver")

func spawnHelicopter():
	if airspace[0] == null or airspace[1] == null:
		var newhelicopter = helicopter.instantiate()
		
		# Vertical position
		if airspace[0] == null:
			airspace[0] = newhelicopter
			newhelicopter.elevationlevel = 0
		else:
			airspace[1] = newhelicopter
			newhelicopter.elevationlevel = 1
		
		# Horizontal position
		randomize()
		if randi() % 2 == 0:
			newhelicopter.direction = 1
		else:
			newhelicopter.direction = -1
		
		newhelicopter.start()
		add_child(newhelicopter)
	
	$HelicopterTimer.wait_time = randi_range(2, 5)
