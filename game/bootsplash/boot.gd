extends Node

@export
var enabled = true

func _ready():
	if enabled:
		$BootTimer.start()
	else:
		boot()

func boot():
	get_tree().change_scene("res://game/game.tscn")
