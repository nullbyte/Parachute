extends Area2D

var velocity = Vector2.ZERO

func _physics_process(delta: float) -> void:
	position += velocity * delta


func kill() -> void:
	queue_free()