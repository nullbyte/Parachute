extends Area2D

@export
var SPEED = 20
var droppedtroopers = 0
var direction = 0
var elevationlevel = 0

var trooperdata = preload("res://objects/paratrooper/paratrooper.tscn")
var blacksquaredata = preload("res://objects/blacksquare/blacksquare.tscn")

func start():
	$FlyAnimation.current_animation = "HelicopterFly"

	position.y = $SizeRect.shape.extents.y * (elevationlevel * 2 + 1)
	
	if direction > 0:
		position.x = -$SizeRect.shape.extents.x
	elif direction < 0:
		$BladesSprite.position.x = -$BladesSprite.position.x
		$HelicopterSprite.flip_h = true
		position.x = 320 + $SizeRect.shape.extents.x

func _physics_process(delta):
	position += Vector2(direction * SPEED * delta, 0)
	if not $Deleter.is_on_screen():
		queue_free()

func onAreaEntered(area: Area2D):
	if "Bullet" in area.name or "BlackSquare" in area.name:
		Signals.emit_signal("hitTarget", false)
		area.queue_free()
		for i in range(5):
			var newblacksquare = blacksquaredata.instantiate()
			call_deferred("add_sibling", newblacksquare)
			newblacksquare.position = position
			randomize()
			newblacksquare.velocity = Vector2(randi_range(0, 50 * direction), randi_range(0, 50))
			randomize()
			newblacksquare.scale.x = randf_range(1, 3)
			randomize()
			newblacksquare.scale.y = randf_range(1, 3)
		queue_free()

func trooperTimer():
	var xpos = snapped(position.x, 8) - 4
	if xpos > 0 and xpos < 320:
		randomize()
		if randi() % 8 == 0:
			var newtrooper = trooperdata.instantiate()
			newtrooper.position.x = xpos
			newtrooper.position.y = position.y + 8
			
			add_sibling(newtrooper)
