extends Area2D

@export
var SPEED = 12

var troopertexture = load("res://objects/paratrooper/trooper.svg")
var parachutetexture = load("res://objects/paratrooper/parachute.svg")

var state = "safefalling"
var landedbody = null
var haslanded = false
var velocity = Vector2.ZERO

func _ready():
	velocity = Vector2(0, SPEED * 3)
	randomize()
	$LaunchTimer.wait_time = randf() + 0.2
	$LaunchTimer.start()

func launchParachute():
	velocity = Vector2(0, SPEED)
	toggleParachute(true)
	state = "floating"

func _physics_process(delta: float):
	if $LandDetector.is_colliding():
		var collider = $LandDetector.get_collider()
		if "Paratrooper" in collider.name:
			if state == "falling":
				Signals.emit_signal("hitTarget", true)
				collider.queue_free()
				Signals.emit_signal("hitTarget", false)
				queue_free()
			else:
				state = "landedbody"
				global_position.y = collider.global_position.y - collider.get_node("Collider").shape.extents.y * 2
				if not haslanded:
					haslanded = true
					Signals.emit_signal("trooperLand")
		elif collider.name == "Floor":
			state = "landedfloor"
			if not haslanded:
				haslanded = true
				Signals.emit_signal("trooperLand")
	elif "landed" in state:
		state = "safefalling"
	
	if "falling" in state:
		toggleParachute(false)
		velocity = Vector2(0, SPEED * 3)
	elif state == "floating":
		toggleParachute(true)
		velocity = Vector2(0, SPEED)
	elif "landed" in state:
		toggleParachute(false)
		set_collision_layer_value(7, true)
	
	if not "landed" in state:
		set_collision_layer_value(7, false)
		position += velocity * delta

func onHitParachute(area: Area2D):
	if area.name == "Bullet" and state == "floating":
		state = "falling"
		area.queue_free()

func onAreaEntered(area: Area2D):
	if "Bullet" in area.name or "BlackSquare" in area.name:
		Signals.emit_signal("hitTarget", "landed" in state)
		area.queue_free()
		queue_free()
	elif area.name == "Floor" and state == "falling":
		Signals.emit_signal("hitTarget", false)
		queue_free()

func toggleParachute(enabled: bool):
	if enabled:
		$Sprite.texture = parachutetexture
	else:
		$Sprite.texture = troopertexture
