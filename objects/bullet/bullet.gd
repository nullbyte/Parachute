extends Area2D

var SPEED = 75
var velocity = Vector2.ZERO

func _physics_process(delta):
	position += velocity * SPEED * delta
	if not $Deleter.is_on_screen():
		queue_free()
