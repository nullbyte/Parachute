extends Node2D

@export
var SPEED = 3
var bullet = preload("res://objects/bullet/bullet.tscn")

func _physics_process(delta):
	rotation += SPEED * delta * (Input.get_action_strength("right") - Input.get_action_strength("left"))
	rotation = clampf(rotation, -PI/2, PI/2)
	
	if Input.is_action_just_pressed("shoot"):
		Signals.emit_signal("shot")
		var newbullet = bullet.instantiate()
		add_sibling(newbullet)

		newbullet.global_position = $Snoot/Shooter.global_position
		newbullet.velocity = Vector2(0, -1).rotated(rotation)
